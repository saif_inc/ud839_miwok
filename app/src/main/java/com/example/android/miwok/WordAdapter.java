package com.example.android.miwok;

import android.app.Activity;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by mustaphar on 3/6/17.
 */

public class WordAdapter extends ArrayAdapter<Word> {

    private int colorid;

    public WordAdapter(Activity activity, ArrayList<Word> words, int colorid) {
        super(activity, 0, words);
        this.colorid = colorid;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        listItemView.setBackgroundResource(colorid);

        Word current = getItem(position);

        TextView tv_defaultTranslation = (TextView) listItemView.findViewById(R.id.word_of_interest);
        tv_defaultTranslation.setText(current.getmDefaultTranslation());

        TextView tv_miwokTranslation = (TextView) listItemView.findViewById(R.id.translation);
        tv_miwokTranslation.setText(current.getmMiwokTranslation());

        ImageView img = (ImageView) listItemView.findViewById(R.id.word_img);
        if (current.isImg())
            img.setImageResource(current.getImageResourceID());
        else
            img.setVisibility(View.GONE);

        return listItemView;
    }
}
