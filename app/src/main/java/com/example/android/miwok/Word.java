package com.example.android.miwok;

/**
 * Created by mustaphar on 3/6/17.
 */

public class Word {
    private String mDefaultTranslation;
    private String mMiwokTranslation;
    private Boolean showImg;
    private int imageResourceId;
    private int audioResourceId;

    public Word(String mDefaultTranslation, String mMiwokTranslation, int audioResourceID)
    {
        showImg = false;
        this.mDefaultTranslation = mDefaultTranslation;
        this.mMiwokTranslation = mMiwokTranslation;
        this.audioResourceId = audioResourceID;
    }

    public Word(String mDefaultTranslation, String mMiwokTranslation, int imageResourceID, int audioResourceID) {
        showImg = true;
        this.mDefaultTranslation = mDefaultTranslation;
        this.mMiwokTranslation = mMiwokTranslation;
        this.imageResourceId = imageResourceID;
        this.audioResourceId = audioResourceID;
    }

    public String getmDefaultTranslation() {
        return mDefaultTranslation;
    }
    public String getmMiwokTranslation() {
        return mMiwokTranslation;
    }
    public boolean isImg() { return showImg; }
    public int getImageResourceID() { return imageResourceId; }
    public int getAudioResourceID() { return audioResourceId; }

    @Override
    public String toString() {
        return "Word{" +
                "mDefaultTranslation='" + mDefaultTranslation + '\'' +
                ", mMiwokTranslation='" + mMiwokTranslation + '\'' +
                ", showImg=" + showImg +
                ", imageResourceId=" + imageResourceId +
                ", audioResourceId=" + audioResourceId +
                '}';
    }

}